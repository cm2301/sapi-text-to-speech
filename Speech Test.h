#pragma once

#include <sapi.h>
#include "string.h"
#include "resource.h"
#include <strsafe.h>             // StringCchCopy, StringCchCopyN
#include <atlbase.h>
#include <atlconv.h>