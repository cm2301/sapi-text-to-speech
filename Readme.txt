You need to install Speech SDK:
http://www.microsoft.com/en-us/download/details.aspx?id=10121

Setup VS Project:

Project > Properties
C/C++ > Additional Include Directories: C:\Program Files (x86)\Microsoft Speech SDK 5.1\Include
Linker > Additional Library Directories: C:\Program Files (x86)\Microsoft Speech SDK 5.1\Lib\i386